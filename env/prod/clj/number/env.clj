(ns number.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[number started successfully]=-"))
   :stop
   (fn []
     (log/info "\n-=[number has shut down successfully]=-"))
   :middleware identity})
