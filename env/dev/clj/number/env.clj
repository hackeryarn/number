(ns number.env
  (:require
    [selmer.parser :as parser]
    [clojure.tools.logging :as log]
    [number.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[number started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[number has shut down successfully]=-"))
   :middleware wrap-dev})
