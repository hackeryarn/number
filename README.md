# number

Number string generator that can check length and sub string occurrences in the
generated string.

## Routes

The app is publicly available at: https://number-demo.herokuapp.com/.

Staging environment for the app is available at: https://number-demo-staging.herokuapp.com/.

To access the swagger API, navigate to:

```
/api/swagger-ui
```

## Developing

### Prerequisites

You will need [Leiningen][1] 2.0 or above installed.

[1]: https://github.com/technomancy/leiningen

### Running

To start a web server for the application, run:

```bash
$ lein run 
```

### Testing

To test the application, run:

```bash
$ lein test 
```

## License

Copyright © 2019 Artem Chernyak
