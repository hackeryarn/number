FROM openjdk:8-alpine

COPY target/uberjar/number.jar /number/app.jar

EXPOSE 3000

CMD ["java", "-jar", "/number/app.jar"]
