(ns number.test.numbers
  (:require
   [number.numbers :as nums]
   [clojure.test :refer :all]))

(deftest tens
  (testing "starts with correct numbers"
    (is (= '("twenty" "twentyone")
           (take 2 (nums/tens)))))
  (testing "transitions between tens correctly"
    (is (= '("fiftynine" "sixty")
           (take 2 (drop 39 (nums/tens))))))
  (testing "ends correctly"
    (is (= '("ninetynine")
           (take 2 (drop 79 (nums/tens)))))))

(deftest hundreds
  (testing "starts with correct numbers"
    (is (= '("onehundred" "onehundredone")
          (take 2 (nums/hundreds)))))
  (testing "transitions to tens correctly"
    (is (= '("onehundrednineteen" "onehundredtwenty")
           (take 2 (drop 19 (nums/hundreds))))))
  (testing "transitions between hundreds correctly"
    (is (= '("fivehundredninetynine" "sixhundred")
          (take 2 (drop 499 (nums/hundreds))))))
  (testing "ends correctly"
    (is (= '("ninehundredninetynine")
          (take 2 (drop 899 (nums/hundreds)))))))

(deftest numbers->string
  (testing "starts with correct numbers"
    (is (= "onetwo"
           (nums/numbers->string 2))))
  (testing "transitions to tens correctly"
    (is (= "nineteentwentytwentyone"
           (apply str (drop 98 (nums/numbers->string 21))))))
  (testing "transitions to hundreds correctly"
    (is (= "ninetynineonehundredonehundredone"
           (apply str (drop 844 (nums/numbers->string 101)))))))

(deftest probability
  (testing "known probability"
    (is (= 50.0M
           (nums/probability 10 5 1))))
  (testing "rounds to two decimal places"
    (is (= 33.33M
           (nums/probability 3 1 1))))
  (testing "takes match length into account"
    (is (= 100M
          (nums/probability 10 5 2)))))
