(ns number.test.validation
  (:require
   [number.validation :as v]
   [clojure.test :refer :all]))

(deftest validate-numbers
  (testing "passes validation on valid input"
    (is (= nil
           (v/validate-numbers {:number 123 :match "e"}))))
  (testing "passes with multi letter match"
    (is (= nil
           (v/validate-numbers {:number 123 :match "two"}))))
  (testing "number and match are required"
    (is (= {:number "this field is mandatory"
            :match "this field is mandatory"}
           (v/validate-numbers {}))))
  (testing "number must be an integer"
    (is (= {:number "must be a integer"}
           (v/validate-numbers {:number "123" :match "e"}))))
  (testing "number can't be smaller than one"
    (is (= {:number "must be between 1 and 999"}
           (v/validate-numbers {:number 0 :match "e"}))))
  (testing "number can't be larger than 999"
    (is (= {:number "must be between 1 and 999"}
           (v/validate-numbers {:number 1000 :match "e"}))))
  (testing "match must be a string"
    (is (= {:match "must be a string"}
           (v/validate-numbers {:number 123 :match 3}))))
  (testing "match can't contain numbers"
    (is (= {:match "must only contain letters"}
           (v/validate-numbers {:number 123 :match "e2"}))))
  (testing "match can't contain special characters"
    (is (= {:match "must only contain letters"}
           (v/validate-numbers {:number 123 :match "e-"})))))
