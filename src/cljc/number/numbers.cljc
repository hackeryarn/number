(ns number.numbers
  (:require [struct.core :as st]
            [clojure.string :as str]))

(def units
  ["one"
   "two"
   "three"
   "four"
   "five"
   "six"
   "seven"
   "eight"
   "nine"
   "ten"
   "eleven"
   "twelve"
   "thirteen"
   "fourteen"
   "fifteen"
   "sixteen"
   "seventeen"
   "eighteen"
   "nineteen"])

(def tens-prefixes
  ["twenty"
   "thirty"
   "forty"
   "fifty"
   "sixty"
   "seventy"
   "eighty"
   "ninety"])

(defn padded-units []
  (cons "" units))

(defn tens []
  (for [x tens-prefixes
        y (take 10 (padded-units))]
    (str x y)))

(defn hundreds []
  (let [single-digits (take 9 units)]
    (for [x single-digits
          y (concat (padded-units) (tens))]
      (str x "hundred" y))))

(defn numbers->string
  "Creates a string of numbers in their English representation. Only prints numbers up to 999."
  [n]
  (->> (hundreds)
       (concat units (tens))
       (take n)
       str/join))

(defn count-occurences
  "Counts the number of times the provided string occurs."
  [match s]
  (->> s (re-seq (re-pattern match)) count))

(defn probability
  "Caclulates the probability in percentage. The results is rounded to a precision of 4. match-length takes the match length into account when calculating probability."
  [total matches match-length]
  (with-precision 4 (* 100M (/ matches (/ total match-length)))))
