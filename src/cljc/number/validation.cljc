(ns number.validation
  (:require [struct.core :as st]))

(def numbers-schema
  [[:number
    st/required
    st/integer
    {:message "must be between 1 and 999"
     :validate (fn [n] (and (<= n 999) (> n 0)))}]
   [:match
    st/required
    st/string
    {:message "must only contain letters"
     :validate #(re-matches #"[a-zA-Z]+" %)}]])

(defn validate-numbers [params]
  (first (st/validate params numbers-schema)))
