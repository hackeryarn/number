(ns number.routes.services
  (:require
   [reitit.swagger :as swagger]
   [reitit.swagger-ui :as swagger-ui]
   [reitit.ring.coercion :as coercion]
   [reitit.coercion.spec :as spec-coercion]
   [reitit.ring.middleware.muuntaja :as muuntaja]
   [reitit.ring.middleware.exception :as exception]
   [reitit.ring.middleware.multipart :as multipart]
   [reitit.ring.middleware.parameters :as parameters]
   [number.validation :refer [validate-numbers]]
   [number.numbers :as nums]
   [number.middleware :as middleware]
   [ring.util.http-response :as response]
   [number.middleware.formats :as formats]))


(defn- get-results
  "Gets the results for number request"
  [{:keys [number match]}]
  (let [num-string (nums/numbers->string number)
        string-length (count num-string)
        match-count (nums/count-occurences match num-string)
        probability (nums/probability string-length match-count (count match))]
    {:stringLength (count num-string) :matchCount match-count :probability probability}))

(defn- number-handler [params]
  (if-let [errors (validate-numbers params)]
    (throw (ex-info "Numbers request is invalid"
                    {:numbers/error-id :validation
                     :errors errors}))
    (get-results params)))

(defn service-routes []
  ["/api"
   {:middleware [parameters/parameters-middleware
                 muuntaja/format-negotiate-middleware
                 muuntaja/format-response-middleware
                 exception/exception-middleware
                 muuntaja/format-request-middleware
                 coercion/coerce-response-middleware
                 coercion/coerce-request-middleware
                 multipart/multipart-middleware]
    :muuntaja formats/instance
    :coercion spec-coercion/coercion
    :swagger {:id ::api}}
   ["" {:no-doc true}
    ["/swagger.json"
     {:get (swagger/create-swagger-handler)}]
    ["/swagger-ui*"
     {:get (swagger-ui/create-swagger-ui-handler
            {:url "/api/swagger.json"})}]]
   ["/numbers"
    {:post
     {:parameters
      {:body
       {:number pos-int?
        :match string?}}
      :responses
      {200
       {:body
        {:stringLength pos-int?
         :matchCount int?
         :probability decimal?}}
       500
       {:errors map?}}
      :handler
      (fn [{{params :body} :parameters}]
        (try
          (response/ok (number-handler params))
          (catch Exception e
            (let [{id :numbers/error-id
                   errors :errors} (ex-data e)]
              (case id
                :validation
                (response/bad-request {:errors errors})
                (response/internal-server-error
                 {:errors {:server-error ["Failed to calculate numbers"]}}))))))}}]])


