(ns number.events
  (:require
   [number.validation :refer [validate-numbers]]
   [clojure.walk :refer [keywordize-keys]]
   [re-frame.core :as rf]
   [ajax.core :as ajax]))

;;dispatchers

(rf/reg-event-db
  :navigate
  (fn [db [_ route]]
    (assoc db :route route)))

(rf/reg-event-fx
 :app/initialize
 (fn [{:keys [db]} _]
   {:db (assoc db :form/fields {:number 123 :match "e"})
    :dispatch [:form/set-ready]}))

(rf/reg-event-db
 :form/set-ready
 [(rf/path :form/loaded?)]
 (fn [_ _]
   true))

(rf/reg-event-db
 :form/set-field
 [(rf/path :form/fields)]
 (fn [fields [_ id value]]
   (assoc fields id value)))

(rf/reg-event-db
 :form/clear-fields
 [(rf/path :form/fields)]
 (fn [_ _]
   {}))

(rf/reg-event-db
  :form/set-server-errors
  [(rf/path :form/server-errors)]
  (fn [_ [_ errors]]
    errors))

(rf/reg-event-db
 :results/set-results
 (fn [db [_ results]]
   (-> db
       (assoc :results/values (keywordize-keys results)
              :results/loading? false))))


(rf/reg-event-fx
 :results/fetch-results
 (fn [{:keys [db]} [_ fields]]
   {:db (assoc db :results/loading? true)
    :http-xhrio {:method          :post
                 :uri             "/api/numbers"
                 :params          fields
                 :format          (ajax/transit-request-format)
                 :response-format (ajax/json-response-format)
                 :on-success      [:results/set-results]}}))

;;subscriptions

(rf/reg-sub
  :route
  (fn [db _]
    (-> db :route)))

(rf/reg-sub
  :page
  :<- [:route]
  (fn [route _]
    (-> route :data :name)))

(rf/reg-sub
 :form/fields
 (fn [db _]
   (:form/fields db)))

(rf/reg-sub
 :form/field
 :<- [:form/fields]
 (fn [fields [_ id]]
   (get fields id)))

(rf/reg-sub
 :form/server-errors
 (fn [db _]
   (:form/server-errors db)))

(rf/reg-sub
 :form/validation-errors
 :<- [:form/fields]
 (fn [fields _]
   (validate-numbers fields)))

(rf/reg-sub
 :form/validation-errors?
 :<- [:form/validation-errors]
 (fn [errors _]
   (not (empty? errors))))

(rf/reg-sub
 :form/errors
 :<- [:form/validation-errors]
 :<- [:form/server-errors]
 (fn [[validation server] _]
   (merge validation server)))

(rf/reg-sub
 :form/error
 :<- [:form/errors]
 (fn [errors [_ id]]
   (get errors id)))

(rf/reg-sub
 :form/loaded?
 (fn [db _]
   (:form/loaded? db)))

(rf/reg-sub
 :results/values
 (fn [db _]
   (:results/values db)))

(rf/reg-sub
 :results/loading?
 (fn [db _]
   (:results/loading? db)))
