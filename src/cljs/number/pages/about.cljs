(ns number.pages.about
 (:require
   [re-frame.core :as rf]))

(defn about-page []
  [:section.section>div.container>div.content
   [:img {:src "/img/warning_clojure.png"}]])
