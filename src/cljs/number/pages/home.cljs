(ns number.pages.home
  (:require
   [clojure.string :as string]
   [re-frame.core :as rf]
   [reagent.core :as r]))

(defn errors-component [id]
  (when-let [error @(rf/subscribe [:form/error id])]
    [:div.notification.is-danger (string/join error)]))

(defn number-slider []
  (let [val (rf/subscribe [:form/field :number])
        draft (r/atom nil)
        value (r/track #(or @draft @val ""))]
    (fn []
      [:div.field
       [:label.label {:for :number} (str "Number: " @value)]
       [errors-component :number]
       [:input.slider
        {:step 1
         :style {:width "100%"}
         :min 0
         :max 999
         :on-focus #(reset! draft (or @val ""))
         :on-blur (fn []
                    (rf/dispatch [:form/set-field
                                  :number
                                  (or (int @draft) "")])
                    (reset! draft nil))
         :on-change #(reset! draft (.. % -target -value))
         :value @value
         :name :number
         :type "range"}]])))


(defn text-input [{val :value
                   attrs :attrs
                   :keys [on-save]}]
  (let [draft (r/atom nil)
        value (r/track #(or @draft @val ""))]
    (fn []
      [:input.input
       (merge attrs
              {:type :text
               :on-focus #(reset! draft (or @val ""))
               :on-blur (fn []
                          (on-save (or @draft ""))
                          (reset! draft nil))
               :on-change #(reset! draft (.. % -target -value))
               :value @value})])))

(defn results []
  (when-let [{:keys [stringLength matchCount probability]} @(rf/subscribe [:results/values])]
    [:div.section.section>div.container>div.content.is-size-5.is-uppercase.has-text-weight-medium
     [:div.columns
      [:div.column
       [:p "Length of string:"]]
      [:div.column
       [:p (str stringLength)]]]
     [:div.columns
      [:div.column
       [:p "Number of matches: "]]
      [:div.column
       [:p (str matchCount)]]]
     [:div.columns
      [:div.column
       [:p "Probability of match: "]]
      [:div.column
       [:p (str probability)]]]]))

(defn numbers-form []
  [:div
   [number-slider]
   [:div.field
    [:label.label {:for :match} "String match to count"]
    [errors-component :match]
    [text-input {:attrs {:name :match}
                 :value (rf/subscribe [:form/field :match])
                 :on-save #(rf/dispatch [:form/set-field :match %])}]]
   [:input.button.is-primary
    {:type :submit
     :disabled (or @(rf/subscribe [:form/validation-errors?])
                   @(rf/subscribe [:results/loading?]))
     :on-click #(rf/dispatch [:results/fetch-results
                              @(rf/subscribe [:form/fields])])
     :value "Calculate Results"}]])

(defn home-page []
  [:div.content>div.columns.is-centered>div.column.is-two-thirds
   [:section.section>div.container>div.content
    (if @(rf/subscribe [:form/loaded?])
      [numbers-form]
      [:h3 "Loading ..."])]
   (if @(rf/subscribe [:results/loading?])
     [:h3 "Loading Results ..."]
     [results])])

